import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {

  inputUsername: string = "";
  allowButton: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  onUpdateUsername(event: any) {
    if ((<HTMLInputElement>event.target).value.length > 0) {
      this.allowButton = true;
    } else {
      this.allowButton = false;
    }
  }

  onSubmit(event: any) {
    this.inputUsername = "";
    this.allowButton = false;
  }

}
