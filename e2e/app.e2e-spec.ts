import { SecondAssignmentPage } from './app.po';

describe('second-assignment App', () => {
  let page: SecondAssignmentPage;

  beforeEach(() => {
    page = new SecondAssignmentPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
